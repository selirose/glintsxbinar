const express = require('express') // Import express
const router = express.Router() // Make router from app
const BarangController = require('../controllers/barangController.js') 
const BarangValidator = require('../middlewares/validators/barangValidator.js') 

router.get('/', BarangController.getAll) 
router.get('/:id', BarangController.getOne) 
router.post('/create', BarangValidator.create, BarangController.create) 
router.put('/update/:id', BarangValidator.update, BarangController.update) 
router.delete('/delete/:id', BarangController.delete) 

module.exports = router; // Export router
