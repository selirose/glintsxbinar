const express = require('express') // Import express
const router = express.Router() // Make router from app
const PemasokController = require('../controllers/pemasokController.js') 
const PemasokValidator = require('../middlewares/validators/pemasokValidator.js') 

router.get('/', PemasokController.getAll) 
router.get('/:id', PemasokController.getOne) 
router.post('/create', PemasokValidator.create, PemasokController.create) 
router.put('/update/:id', PemasokValidator.update, PemasokController.update) 
router.delete('/delete/:id', PemasokController.delete) 

module.exports = router; // Export router