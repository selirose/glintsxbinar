// Import Mongodb
const {MongoClient} = require('mongodb');

// setup URI (Uniform Resource Identifier) connection
// This connection string is needed to specify the connection parameters like the URL, port, username, password for the Database
const uri = 'mongodb://localhost:27017' // URI of mongodb in our computer
const client = new MongoClient(uri, {useUnifiedTopology: true})  // Create new MongoClient

client.connect();

module.exports = client;
