const client = require('../models/connection.js')
const {ObjectId} = require('mongodb')

const penjualan = client.db('penjualan')
const pemasok = penjualan.collection('pemasok')

class PemasokController {

  async getAll(req,res) {
    return pemasok.find({}).toArray().then(r => {
      res.json({
        status:'success',
        data:r
      })
    })
  }

  async getOne(req,res) {
    await pemasok.findOne({
      _id:new ObjectId(req.params.id)
    }).then(r=>{
      res.json({
        status:'success',
        data:r
      })
    })
  }

  async create(req,res) {
    return pemasok.insertOne({
      nama:req.body.nama
    }).then(r=>{
      res.json({
        status:'success',
        data:r.ops[0]
      })
    })
  }

  async update(req,res) {
    return pemasok.updateOne({
      _id:new ObjectId(req.params.id)
    }, {
      $set: {
        nama :req.body.nama
      }
    }).then(() => {
      return pemasok.findOne({
        _id:new ObjectId(req.params.id)
      })
    }).then(result => {
      res.json({
        status:'success',
        data: result
      })
    })
  }

  async delete(req,res) {
    return pemasok.deleteOne({
      _id: new ObjectId(req.params.id)
    }).then(result =>{
      res.json({
        status:'success',
        data:null
      })
    })
  }

}

module.exports = new PemasokController
