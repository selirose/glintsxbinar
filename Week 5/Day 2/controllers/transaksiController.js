const client = require('../models/connection.js')
const {ObjectId} = require('mongodb')

const penjualan = client.db('penjualan')
const transaksi = penjualan.collection('transaksi')

class TransaksiController {

  async getAll(req,res) {
    return transaksi.find({}).toArray().then(result =>{
      res.json({
        status: 'success',
        data:result
      })
    })
  }

  async getOne(req,res) {
    await transaksi.findOne({
      _id: new ObjectId(req.params.id)
    }).then(result => {
      res.json({
        status:'success',
        data:result
      })
    })
  }

  async create(req,res) {

    const barang = await penjualan.collection('barang').findOne({
      _id: new ObjectId(req.body.id_barang)
    })

    const pelanggan = await penjualan.collection('pelanggan').findOne({
      _id: new ObjectId(req.body.id_pelanggan)
    })

    let total = eval(barang.harga.toString()) * req.body.jumlah

    return transaksi.insertOne({
      barang: barang,
      pelanggan: pelanggan,
      jumlah: req.body.jumlah,
      total: total
    }).then(result => {
      res.json({
        status:'success',
        data:result.ops[0]
      })
    })
  }

  async update(req,res) {

    const barang = await penjualan.collection('barang').findOne({
      _id: new ObjectId(req.body.id_barang)
    })

    const pelanggan = await penjualan.collection('pelanggan').findOne({
      _id: new ObjectId(req.body.id_pelanggan)
    })

    let total = eval(barang.harga.toString()) * req.body.jumlah

    return transaksi.updateOne({
      _id: new ObjectId(req.params.id)
    }, {
      $set: {
        barang: barang,
        pelanggan: pelanggan,
        jumlah: req.body.jumlah,
        total: total
      }
    }).then(() =>{
      return transaksi.findOne({
        _id: new ObjectId(req.params.id)
      })
    }).then(result => {
      res.json({
        status: 'success',
        data:result
      })
    })
  }

  async delete(req,res) {
    return transaksi.deleteOne({
      _id: new ObjectId(req.params.id)
    }).then(result =>{
      res.json({
        status:'success',
        data:null
      })
    })
  }
}

module.exports = new TransaksiController
