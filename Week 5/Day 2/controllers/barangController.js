const client = require('../models/connection.js')
const {ObjectId} = require('mongodb')

const penjualan = client.db('penjualan')
const barang = penjualan.collection('barang')

class BarangController{

  async getAll(req,res) {
    return barang.find({}).toArray().then(r=>{
      res.json({
        status:'success',
        data:r
      })
    })
  }

  async getOne(req,res) {
    await barang.findOne({
      _id: new ObjectId(req.params.id)
    }).then(r=> {
      res.json({
        status:'success',
        data:r
      })
    })
  }

  async create(req,res) {
    const pemasok = await penjualan.collection('pemasok').findOne({
      _id: new ObjectId(req.body.id_pemasok)
    })
    const check = await barang.findOne({
      nama:req.body.nama,
      id_pemasok:req.body.id_pemasok
    }).then(result=> {
      if(result) {
        return res.json({
          status:'failed',
          message:'Nama dan id_pemasok barang sudah ada',
          data:result
        })
      } else {
        return barang.insertOne({
          nama: req.body.nama,
          harga:req.body.harga,
          pemasok:pemasok
        }).then(r=>{
          res.json({
            status:'success',
            data:r.ops[0]
          })
        })
      }
    })
  }

  async update(req,res) {
    const pemasok = await penjualan.collection('pemasok').findOne({
      _id: new ObjectId(req.body.id_pemasok)
    })
    const check = await barang.findOne({
      nama:req.body.nama,
      id_pemasok:req.body.id_pemasok
    }).then(result=> {
      if(result) {
        return res.json({
          status:'failed',
          message:'Nama dan id_pemasok barang sudah ada',
          data:result
        })
      } else {
        return barang.updateOne({
          _id:new ObjectId(req.params.id)
        },{
          $set: {
            nama: req.body.nama,
            harga:req.body.harga,
            pemasok:pemasok
          }
        }).then(() =>{
          return barang.findOne({
            _id:new ObjectId(req.params.id)
          })
        }).then(r=>{
          res.json({
            status:'success',
            data:r
          })
        })
      }
    })
  }

  async delete(req,res) {
    return barang.deleteOne({
      _id: new ObjectId(req.params.id)
    }).then(result =>{
      res.json({
        status:'success',
        data:null
      })
    })
  }

}

module.exports = new BarangController
