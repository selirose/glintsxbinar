const client = require('../models/connection.js')
const {ObjectId} = require('mongodb')

const penjualan = client.db('penjualan')
const pelanggan = penjualan.collection('pelanggan')

class PelangganController {

  async getAll(req,res) {
    return pelanggan.find({}).toArray().then(r => {
      res.json({
        status:'success',
        data:r
      })
    })
  }

  async getOne(req,res) {
    await pelanggan.findOne({
      _id:new ObjectId(req.params.id)
    }).then(r=>{
      res.json({
        status:'success',
        data:r
      })
    })
  }

  async create(req,res) {
    return pelanggan.insertOne({
      nama:req.body.nama
    }).then(r=>{
      res.json({
        status:'success',
        data:r.ops[0]
      })
    })
  }

  async update(req,res) {
    return pelanggan.updateOne({
      _id:new ObjectId(req.params.id)
    }, {
      $set: {
        nama :req.body.nama
      }
    }).then(() => {
      return pelanggan.findOne({
        _id:new ObjectId(req.params.id)
      })
    }).then(result => {
      res.json({
        status:'success',
        data: result
      })
    })
  }

  async delete(req,res) {
    return pelanggan.deleteOne({
      _id: new ObjectId(req.params.id)
    }).then(result =>{
      res.json({
        status:'success',
        data:null
      })
    })
  }

}

module.exports = new PelangganController
