const {check, validationResult, matchedData, sanitize} = require('express-validator');
const client = require('../../models/connection.js')
const {ObjectId} =require('mongodb')

module.exports = {

  create: [
    check('id_pemasok').custom(value => {
      return client.db('penjualan').collection('pemasok').findOne({
        _id : new ObjectId(value)
      }).then(result => {
        if (!result) {
          throw new Error('ID Pemasok tidak ada');
        }
      })
    }),
    check('nama').isAlpha(),
    check('harga').isLength({min:3}).isNumeric(),
    (req,res,next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],

  update: [
    check('id').custom(value => {
      return client.db('penjualan').collection('barang').findOne({
        _id: new ObjectId(value)
      }).then(result => {
        if (!result) {
          throw new Error('ID Barang tidak ada')
        }
      })
    }),
    check('id_pemasok').custom(value => {
      return client.db('penjualan').collection('pemasok').findOne({
        _id : new ObjectId(value)
      }).then(result => {
        if (!result) {
          throw new Error('ID Pemasok tidak ada');
        }
      })
    }),
    check('nama').isAlpha(),
    check('harga').isLength({min:3}).isNumeric(),
    (req,res,next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],

  delete: [
    check('id').custom(value => {
      return client.db('penjualan').collection('barang').findOne({
        _id: new ObjectId(value)
      }).then(result => {
        if (!result) {
          throw new Error('ID Barang tidak ada')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    }
  ]

}
