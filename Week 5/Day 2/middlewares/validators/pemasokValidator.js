const {check, validationResult, matchedData, sanitize} = require('express-validator');
const client = require('../../models/connection.js')
const {ObjectId} =require('mongodb')

module.exports = {

  create: [
    check('nama').isAlpha().custom(value=> {
      return client.db('penjualan').collection('pemasok').findOne({
        nama: value
      }).then(result => {
        if(result){
          throw new Error('Nama pemasok sudah ada')
        }
      })
    }),
    (req,res,next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],

  update: [
    check('id').custom(value => {
      return client.db('penjualan').collection('pemasok').findOne({
        _id: new ObjectId(value)
      }).then(result => {
        if (!result) {
          throw new Error('ID Transaksi tidak ada')
        }
      })
    }),
    check('nama').isAlpha().custom(value=> {
      return client.db('penjualan').collection('pemasok').findOne({
        nama: value
      }).then(result => {
        if(result) {
          throw new Error('Nama Pemasok sudah ada')
        }
      })
    }),
    (req,res,next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],

  delete: [
    check('id').custom(value => {
      return client.db('penjualan').collection('pemasok').findOne({
        _id: new ObjectId(value)
      }).then(result => {
        if (!result) {
          throw new Error('ID Pemasok tidak ada')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    }
  ]
}
