const express = require('express')
const router = express.Router()
const seliController = require('../controllers/selicontroller.js')

router.get('/', seliController.seli)

module.exports = router;