const express = require('express')
const app = express()
const seliRoutes = require('./routes/seliRoutes.js')

app.use('/seli', seliRoutes)
app.listen(3000)