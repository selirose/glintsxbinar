const fs = require ('fs')

const readFile = option => file => new Promise ((resolve, reject) => {
    fs.readFile(file, option,(err, content) =>{
        if (err) return reject (err)
        return resolve(content)
    })
}) 

const writeFile = (file, content) => new Promise ((resolve, reject) =>{
    fs.writeFile(file, content, err => {
        if (err) return reject(err)
        return resolve()
    })
})

const read = readFile('utf-8')

//Method Async Await
async function mergeContent(){
    try {
        const content1 = await read ('content/content1.txt')
        const content2 = await read ('content/content2.txt')
        const content3 = await read ('content/content3.txt')
        const content4 = await read ('content/content4.txt')
        const content5 = await read ('content/content5.txt')
        const content6 = await read ('content/content6.txt')
        const content7 = await read ('content/content7.txt')
        const content8 = await read ('content/content8.txt')
        const content9 = await read ('content/content9.txt')
        const content10 = await read ('content/content10.txt')

        await writeFile('content/result1.txt', content1 + content2 + content3 + content4 + content5 + content6 + content7 + content8 + content9 + content10)
    } catch(e){
        throw e
    }
    return read ('content/result1.txt')
    
}
mergeContent()
    .then(result =>{
        console.log(result);
    }).catch(err =>{
        console.log('Error read/write file, error: err');
    })

//Method Promise all
async function mergeContent1(){
    try{
        const result = await Promise.all([
            read('content/content1.txt'),
            read('content/content2.txt'),
            read('content/content3.txt'),
            read('content/content4.txt'),
            read('content/content5.txt'),
            read('content/content6.txt'),
            read('content/content7.txt'),
            read('content/content8.txt'),
            read('content/content9.txt'),
            read('content/content10.txt'),
        ])
        await writeFile('content/result2.txt', result2.join(''))
    } catch (e){
        throw e
    }

    return read ('content/result2.txt')
}

mergeContent1()
.then(result => {
    console.log(result2)
}).catch(err => {
    console.log('Error to read/write file, error: ', err)
}).finally(() => {
    console.log('Cool!');
})