const { user } = require('../models/mysql') // import user models
const passport = require('passport'); // import passport
const jwt = require('jsonwebtoken'); // import jsonwebtoken

class UserController {

  // if user signup
  async signup(req, res) {
    // get the req.user from passport authentication
    const body = {
      id: req.user.dataValues.id,
      email: req.user.dataValues.email
    };

    // create jwt token from body variable
    const token = jwt.sign({
      user: body
    }, 'secret_password')

    // success to create token
    res.status(200).json({
      message: 'Signup success!',
      token: token
    })
  }

  // if user login
  async login(req, res) {
    // get the req.user from passport authentication
    const body = {
      id: req.user.dataValues.id,
      email: req.user.dataValues.email
    };

    // create jwt token from body variable
    const token = jwt.sign({
      user: body
    }, 'secret_password')

    // success to create token
    res.status(200).json({
      message: 'Login success!',
      token: token
    })
  }

  // get all users
  async getAll(req, res) {
    return user.find({}).toArray().then(result =>{
      res.json({
        status: 'success',
        data:result
      })
    })

  }

}

module.exports = new UserController; // export UserController
