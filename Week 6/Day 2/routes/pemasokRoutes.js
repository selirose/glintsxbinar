const express = require('express') // Import express
const router = express.Router() // Make router from app
const PemasokController = require('../controllers/pemasokController.js') 
const pemasokValidator = require('../middlewares/validators/pemasokValidator.js') 
const passport = require('passport'); // import passport
const auth = require('../middlewares/auth');


router.get('/', [passport.authenticate('pemasok', {
    session: false
})], PemasokController.getAll)

router.get('/:id', [passport.authenticate('pemasok', {
    session: false
}), pemasokValidator.getOne], PemasokController.getOne)

router.post('/create', [passport.authenticate('pemasok', {
    session: false
}), pemasokValidator.create], PemasokController.create)

router.put('/update/:id', [passport.authenticate('pemasok', {
    session: false
}), pemasokValidator.update], PemasokController.update)

router.delete('/delete/:id', [passport.authenticate('pemasok', {
    session: false
}), pemasokValidator.delete], PemasokController.delete)

module.exports = router; 