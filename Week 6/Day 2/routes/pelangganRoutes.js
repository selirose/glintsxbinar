const express = require('express') // Import express
const router = express.Router() // Make router from app
const PelangganController = require('../controllers/pelangganController.js') 
const pelangganValidator = require('../middlewares/validators/pelangganValidator.js') 
const passport = require('passport'); // import passport
const auth = require('../middlewares/auth'); // import passport strategy


router.get('/',  PelangganController.getAll)

// [passport.authenticate('pelanggan', {
//     session: false
// })],

router.get('/:id', [passport.authenticate('pelanggan', {
    session: false
}), pelangganValidator.getOne], PelangganController.getOne)

router.post('/create', [passport.authenticate('pelanggan', {
    session: false
}), pelangganValidator.create], PelangganController.create)

router.put('/update/:id', [passport.authenticate('pelanggan', {
    session: false
}), pelangganValidator.update], PelangganController.update)

router.delete('/delete/:id', [passport.authenticate('pelanggan', {
    session: false
}), pelangganValidator.delete], PelangganController.delete)

module.exports = router; // Export router
