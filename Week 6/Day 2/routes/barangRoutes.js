const express = require('express') // Import express
const router = express.Router() // Make router from app
const BarangController = require('../controllers/barangController.js') 
const barangValidator = require('../middlewares/validators/barangValidator.js') 
const passport = require('passport'); // import passport
const auth = require('../middlewares/auth'); // import passport strategy

router.get('/', [passport.authenticate('barang', {
    session: false
})], BarangController.getAll)

router.get('/:id', [passport.authenticate('barang', {
    session: false
}), barangValidator.getOne], BarangController.getOne)

router.post('/create', [passport.authenticate('barang', {
    session: false
}), barangValidator.create], BarangController.create)

router.put('/update/:id', [passport.authenticate('barang', {
    session: false
}), barangValidator.update], BarangController.update)

router.delete('/delete/:id', [passport.authenticate('barang', {
    session: false
}), barangValidator.delete], BarangController.delete)

module.exports = router; // Export router
