// untuk definisi array boleh pakai var boleh pakai let

// mengerjakan sendiri
let box = ["tomato", "broccoli", "kale", "cabbage", "apple"];
console.log((box[0]) , "is a healthy food, it's definitely worth to eat.");
console.log((box[1]) , "is a healthy food, it's definitely worth to eat.");
console.log((box[2]) , "is a healthy food, it's definitely worth to eat.");
console.log((box[3]) , "is a healthy food, it's definitely worth to eat.");
console.log((box[4]) , "is not a Vegetable");

//kunci jawaban
for (let i = 0; i < box.length; i++){
    if (box[i] !== "apple"){
        console.log(`${box[i]} is a healthy food, it's definitely worth to eat.`)
     }
}
