const data = require('./lib/arrayFactory.js');
const test = require('./lib/test.js');

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter(i => typeof i === 'number');
}

// Should return array
function sortAscending(data) {
    // Code Here
    data = clean(data);
  
    /* Method 1 */
    for (let i =0 ; i < data.length; i ++) {
      for (let j =0; j < data.length - i -1 ; j ++) {
        if (data[j] > data[j+1]) {
          let tmp = data[j];
          data[j] = data[j+1];
          data[j+1] = tmp;
        }
      }
    }
  
    return data;
  }

// Should return array
function sortDecending(data) {
    data = clean(data);
  
    /* Method 1 */
    for (let i =0 ; i < data.length; i ++) {
      for (let j =0; j < data.length - i -1 ; j ++) {
        if (data[j] < data[j+1]) {
          let tmp = data[j];
          data[j] = data[j+1];
          data[j+1] = tmp;
        }
      }
    }
  
    return data;

}

// DON'T CHANGE
test(sortAscending, sortDecending, data);

//console.log(sortAscending(data));
//console.log(sortDecending(data));




