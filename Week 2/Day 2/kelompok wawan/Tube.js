const index = require('./menu_index.js')

const pi = 3.14

function volTube(radius,height){
    return pi*radius**2*height;
}

function inputHeight(){
  index.rl.question(`\nHeight: `, height => {
    if (!isNaN(height) && !index.isEmptyOrSpaces(height)) {
      inputRadius(height)
    } else {
      console.log("Height must be a number");
      inputHeight()
    }
  })
}

function inputRadius(height) {
  index.rl.question('Radius: ', radius =>{
    if (!isNaN(radius) && !index.isEmptyOrSpaces(radius)) {
      console.log(`Tube Volume: ${volTube(radius, height)}`);
      index.inputOption()
    } else {
      console.log(`Radius must be a number\n`);
      inputRadius(height)
    }
  })
}

module.exports.inputHeight = inputHeight
